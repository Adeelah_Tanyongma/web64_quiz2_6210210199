const mysql = require('mysql');

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'cafe',
    password : 'cafe',
    database : 'CafeReviewSystem'
});

connection.connect();

const express = require('express')
const app = express()
const port = 4000


/* Backend for Registering a new Review */

app.post("/register", (req, res) => {

    let review_name = req.query.review_name
    let review_surname = req.query.review_surname

    let query = `INSERT INTO Review
                (ReviewName, ReviewSurname) 
                VALUES ('${review_name}','${review_surname}')`
    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({ 
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding review succesful"
            })
        }    
    });

});


/*app.post("/update_register", (req, res) => {

    let review_name = req.query.review_name
    let review_surname = req.query.review_surname

    let query = `UPDATE Review SET
                    ReviewName = '${review_name}',
                    ReviewSurname = ${review_surname}
                    WHERE ReviewID = ${review_id}`

    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({ 
                        "status" : "400",
                        "message" : "Error updating record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Updating cafe succesful"
            })
        }    
    });

})*/





/* CRUD Operation for RunningEvent Table */
app.get("/select_cafe", (req, res) => {
    let query = "SELECT * FROM ReviewCafe";
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({ 
                        "status" : "400",
                        "message" : "Error querying from review db"
                        })
        }else {
            res.json(rows)
        }   
    });
})

app.post("/insert_cafe", (req, res) => {

    let cafe_name = req.query.cafe_name
    let cafe_location = req.query.cafe_location

    let query = `INSERT INTO ReviewCafe
                (CafeName, CafeLocation) 
                VALUES ('${cafe_name}','${cafe_location}')`
    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({ 
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding review succesful"
            })
        }    
    });

})

app.post("/update_cafe", (req, res) => {

    let cafe_id = req.query.cafe_id
    let cafe_name = req.query.cafe_name
    let cafe_location = req.query.cafe_location

    let query = `UPDATE ReviewCafe SET
                    CafeName = '${cafe_name}',
                    CafeLocation = '${cafe_location}' 
                    WHERE CafeID = ${cafe_id}`

    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({ 
                        "status" : "400",
                        "message" : "Error updating record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Updating cafe succesful"
            })
        }    
    });

})

app.post("/delete_cafe", (req, res) => {

    let cafe_id = req.query.cafe_id

    let query = `DELETE FROM ReviewCafe WHERE cafeID = ${cafe_id}`

    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({ 
                        "status" : "400",
                        "message" : "Error deleting record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Deleting record success"
            })
        }    
    });

})


app.listen(port, () => {
    console.log(`Now starting Review Cafe Backend ${port} `)
})



